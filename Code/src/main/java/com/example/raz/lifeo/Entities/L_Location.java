package com.example.raz.lifeo.Entities;

import java.io.Serializable;

/**
 * Created by Raz on 7/25/2017.
 */
public class L_Location implements Serializable {
    private String provider;
    private double longtitude;
    private double lantitude;


    public L_Location(String provider) {
        this.provider = provider;
    }

    public double getLongitude(){
        return longtitude;
    }
    public void setLongitude(double longtitude){
        this.longtitude = longtitude;
    }
    public double getLatitude(){
        return lantitude;
    }
    public void setLatitude(double lantitude){
        this.lantitude = lantitude;
    }

    public String getProvider(){
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
