package com.example.raz.lifeo.Fragments;


import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.raz.lifeo.Components.LifeoDatePicker;
import com.example.raz.lifeo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginPanel extends Fragment {


    private View m_nameEditText = null;
    private View m_birthdayWidget = null;
    private EditText m_userEditText = null;
    private EditText m_birthdayEditText = null;
    public LoginPanel() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_panel, container, false);
        initUI(view);
        return view;
    }

    private void initUI(View view){
        m_nameEditText = (View) view.findViewById(R.id.login_name_edittext);
        m_birthdayWidget = (View) view.findViewById(R.id.login_birthday_widget);

        //Name EditText:
        m_userEditText = (EditText) m_nameEditText.findViewById(R.id.lifeo_edit_text_text);
        ImageView nameImage = (ImageView) m_nameEditText.findViewById(R.id.lifeo_edit_text_image);
        m_userEditText.setHint(getResources().getString(R.string.login_name_text));
        Drawable nameImageDrawable = getResources().getDrawable(getResources().getIdentifier("@mipmap/name_icon",null,getActivity().getPackageName()));
        nameImage.setImageDrawable(nameImageDrawable);

        //Birthday Widget:
        m_birthdayEditText = (EditText) m_birthdayWidget.findViewById(R.id.lifeo_edit_text_text);
        ImageView birthdayImage = (ImageView) m_birthdayWidget.findViewById(R.id.lifeo_edit_text_image);
        m_birthdayEditText.setHint(getResources().getString(R.string.login_birthday_text));
        Drawable birthdayImageDrawable = getResources().getDrawable(getResources().getIdentifier("@mipmap/birthday_icon", null, getActivity().getPackageName()));
        birthdayImage.setImageDrawable(birthdayImageDrawable);
        m_birthdayEditText.setClickable(true);
        m_birthdayEditText.setFocusable(false);

        View.OnClickListener m_birthdayListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                LifeoDatePicker dataPickerView = new LifeoDatePicker();
                dataPickerView.setDatePicker(m_birthdayEditText);
                dataPickerView.show(getActivity().getFragmentManager(), getActivity().getResources().getString(R.string.default_tag));
            }
        };

        m_birthdayWidget.setOnClickListener(m_birthdayListener);
        m_birthdayEditText.setOnClickListener(m_birthdayListener);
    }

    public String name(){
        return (m_userEditText==null)?null:m_userEditText.getText().toString();
    }

    public String birthday(){
        return (m_birthdayEditText==null)?null:m_birthdayEditText.getText().toString();
    }

}
