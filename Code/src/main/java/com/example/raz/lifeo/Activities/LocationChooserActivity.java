package com.example.raz.lifeo.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.media.Image;
import android.support.annotation.DrawableRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raz.lifeo.Adapters.GooglePlacesAutocompleteAdapter;
import com.example.raz.lifeo.Entities.L_Location;
import com.example.raz.lifeo.Interfaces.Notified;
import com.example.raz.lifeo.R;
import com.example.raz.lifeo.Tools.LifeoRecorder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Text;

import java.io.IOException;
import java.text.Bidi;
import java.util.List;
import java.util.Locale;

public class LocationChooserActivity extends FragmentActivity
        implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        Notified {

    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private GoogleMap mMap;
    private LocationRequest mLocationRequest = null;
    private GoogleApiClient mGoogleApiClient = null;

    private EditText m_fromEditText = null;
    private EditText m_toEditText = null;
    private ListView m_fromListView = null;
    private ListView m_toListView = null;
    private GooglePlacesAutocompleteAdapter m_fromDataAdapter = null;
    private GooglePlacesAutocompleteAdapter m_toDataAdapter = null;

    private ImageButton m_fromRecordBtn = null;
    private ImageButton m_toRecordBtn = null;
    private ImageButton m_fromGeoBtn = null;

    private L_Location m_fromLocation = null;
    private L_Location m_toLocation = null;
    private L_Location m_currentLocation = null;

    private boolean m_start = true;
    private ImageButton m_geoCenterBtn = null;
    private ImageButton m_approveBtn = null;

    private LifeoRecorder m_fromRecorder = null;
    private LifeoRecorder m_toRecorder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_chooser);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setStatusBarTransparentColor();

        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        m_fromRecorder = new LifeoRecorder(this,this, getResources().getString(R.string.location_from_where_tag));
        m_toRecorder = new LifeoRecorder(this,this, getResources().getString(R.string.location_to_where_tag));

        initUI();

    }

    private void checkForIntent() {
        Intent intent = getIntent();
        if(intent.hasExtra(getResources().getString(R.string.tags_event_from))){
            m_fromLocation = (L_Location) intent.getSerializableExtra(getResources().getString(R.string.tags_event_from));
            if(m_fromLocation!=null){
                m_fromEditText.setText(m_fromLocation.getProvider());
                m_fromListView.setVisibility(View.GONE);
            }
        }
        if(intent.hasExtra(getResources().getString(R.string.tags_event_to))){
            m_toLocation = (L_Location) intent.getSerializableExtra(getResources().getString(R.string.tags_event_to));
            if(m_toLocation!=null){
                m_toEditText.setText(m_toLocation.getProvider());
                m_toListView.setVisibility(View.GONE);
                diveToLocation(m_toLocation);
            }
        }
    }

    private void initUI() {
        m_fromEditText = (EditText) findViewById(R.id.location_activity_from_where);
        m_fromListView = (ListView) findViewById(R.id.location_activity_from_listview);
        m_fromRecordBtn = (ImageButton) findViewById(R.id.location_activity_from_record_btn);
        m_fromGeoBtn = (ImageButton) findViewById(R.id.location_activity_from_geo_btn);

        m_toEditText = (EditText) findViewById(R.id.location_activity_to_where);
        m_toListView = (ListView) findViewById(R.id.location_activity_to_listview);
        m_toRecordBtn = (ImageButton) findViewById(R.id.location_activity_to_record_btn);

        m_geoCenterBtn = (ImageButton) findViewById(R.id.location_activity_geo_self_btn);
        m_approveBtn = (ImageButton) findViewById(R.id.location_activity_approve_btn);

        SetListeners();
    }

    private void SetListeners() {
        m_fromDataAdapter = new GooglePlacesAutocompleteAdapter(LocationChooserActivity.this,
                R.layout.adapter_google_places_autocomplete);
        m_toDataAdapter = new GooglePlacesAutocompleteAdapter(LocationChooserActivity.this,
                R.layout.adapter_google_places_autocomplete);

        // Assign adapter to ListView
        m_fromListView.setAdapter(m_fromDataAdapter);
        m_toListView.setAdapter(m_toDataAdapter);
        //enables filtering for the contents of the given ListView
        m_fromListView.setTextFilterEnabled(true);
        m_toListView.setTextFilterEnabled(true);
        //set click item listeners
        m_fromListView.setOnItemClickListener(onItemClickListener1);
        m_toListView.setOnItemClickListener(onItemClickListener2);

        m_fromEditText.addTextChangedListener(fromEditTextWatcher);
        m_toEditText.addTextChangedListener(toEditTextWatcher);

        m_fromGeoBtn.setOnClickListener(geoFromBtnListener);
        m_geoCenterBtn.setOnClickListener(geoCenterBtnListener);
        m_approveBtn.setOnClickListener(approveBtnListener);

        m_fromRecordBtn.setOnTouchListener(fromRecordBtnListener);
        m_toRecordBtn.setOnTouchListener(fromRecordBtnListener);
    }

    private View.OnTouchListener fromRecordBtnListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d("TouchListener","pressed");
                if(v==m_fromRecordBtn){
                    m_fromRecorder.startRecording();
                } else if(v == m_toRecordBtn){
                    m_toRecorder.startRecording();
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                Log.d("TouchListener","released");
                if(v==m_fromRecordBtn){
                    m_fromRecorder.stopRecording();
                } else if(v == m_toRecordBtn){
                    m_toRecorder.stopRecording();
                }
            }
            return true;
        }
    };

    private View.OnClickListener approveBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            if(m_fromLocation!=null){
                intent.putExtra(getResources().getString(R.string.location_from_where_tag), m_fromLocation);
            }
            if(m_toLocation!=null){
                intent.putExtra(getResources().getString(R.string.location_to_where_tag), m_toLocation);
            }
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private View.OnClickListener geoCenterBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            diveToLocation(m_currentLocation);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMap.addMarker(new MarkerOptions().position(
                            new LatLng(m_currentLocation.getLatitude(), m_currentLocation.getLongitude()))
                            .title(getResources().getString(R.string.you_are_here_text)));
                }
            });
        }
    };

    private View.OnClickListener geoFromBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(m_currentLocation==null){
                Toast.makeText(getApplicationContext()
                        ,getResources().getString(R.string.location_activity_doesnt_have_current_location)
                        ,Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                String address = coordinatesToAddress(m_currentLocation.getLatitude(),m_currentLocation.getLongitude());
                m_fromLocation = m_currentLocation;
                m_fromLocation.setProvider(address);
                m_fromEditText.setText(address);
                m_fromListView.setVisibility(View.GONE);
                diveToLocation(m_fromLocation);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private String coordinatesToAddress(double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

        return address;
    }

    private TextWatcher fromEditTextWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            Bidi bidi = new Bidi(m_fromEditText.getText().toString(), Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT);
            if (bidi.getBaseLevel() == 0) {
                setButtons(m_fromGeoBtn, m_fromRecordBtn, true);
            } else {
                setButtons(m_fromGeoBtn, m_fromRecordBtn, false);
            }
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            m_fromListView.setVisibility(View.VISIBLE);
            m_fromDataAdapter.getFilter().filter(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private TextWatcher toEditTextWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            Bidi bidi = new Bidi(m_toEditText.getText().toString(), Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT);
            if (bidi.getBaseLevel() == 0) {
                setButtons(m_toRecordBtn, true);
            } else {
                setButtons(m_toRecordBtn, false);
            }
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            m_toListView.setVisibility(View.VISIBLE);
            m_toDataAdapter.getFilter().filter(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private AdapterView.OnItemClickListener onItemClickListener2 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView textView = (TextView) view.findViewById(R.id.google_auto_compelete_textview);
            if (textView == null) return;
            String address = textView.getText().toString();
            m_toEditText.setText(address);
            m_toListView.setVisibility(View.GONE);
            m_toLocation = getLocation(address);
            diveToLocation(m_toLocation);
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    };

    private L_Location getLocation(String address){
        Geocoder geocoder = new Geocoder(getApplicationContext());
        double latitude = 0;
        double longitude = 0;
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(address, 1);
            if (addresses.size() > 0) {
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        L_Location result = new L_Location(address);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        return result;
    }

    private AdapterView.OnItemClickListener onItemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView textView = (TextView) view.findViewById(R.id.google_auto_compelete_textview);
            if (textView == null) return;
            String address = textView.getText().toString();
            //rewrite from edit text.
            m_fromEditText.setText(address);
            //close listview.
            m_fromListView.setVisibility(View.GONE);
            m_fromLocation = getLocation(address);
            diveToLocation(m_fromLocation);
            //close keyboard
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    };

    private void setButtons(ImageButton toRecordBtn, boolean rtl) {
        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (rtl) {
            lp1.addRule(RelativeLayout.ALIGN_PARENT_END, toRecordBtn.getId());
            toRecordBtn.setLayoutParams(lp1);
        } else {
            lp1.addRule(RelativeLayout.ALIGN_PARENT_START, toRecordBtn.getId());
            toRecordBtn.setLayoutParams(lp1);
        }
    }

    private void setButtons(ImageButton fromGeoBtn, ImageButton fromRecordBtn, boolean rtl) {
        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (rtl) {
            lp1.addRule(RelativeLayout.ALIGN_PARENT_END, fromRecordBtn.getId());
            lp2.addRule(RelativeLayout.LEFT_OF, fromRecordBtn.getId());
            fromRecordBtn.setLayoutParams(lp1);
            fromGeoBtn.setLayoutParams(lp2);
        } else {
            lp1.addRule(RelativeLayout.RIGHT_OF, fromGeoBtn.getId());
            lp2.addRule(RelativeLayout.ALIGN_PARENT_START, fromGeoBtn.getId());
            fromRecordBtn.setLayoutParams(lp1);
            fromGeoBtn.setLayoutParams(lp2);
        }

    }

    private void setStatusBarTransparentColor() {
        Window window = getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.transparent));
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        checkForIntent();
    }


    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onLocationChanged(final Location location) {
        m_currentLocation = new L_Location(location.getProvider());
        m_currentLocation.setLatitude(location.getLatitude());
        m_currentLocation.setLongitude(location.getLongitude());
        if(m_start){
            diveToLocation(m_currentLocation);
            m_start = false;
        }
    }

    public void diveToLocation(final L_Location location){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setMarkers();

                final CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))
                        .zoom(17)
                        .tilt(30)
                        .build();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    private void setMarkers() {
        if(mMap==null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.clear();
                if (m_fromLocation != null) {
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(m_fromLocation.getLatitude(), m_fromLocation.getLongitude()))
                            .title(getResources().getString(R.string.location_activity_from_text))
                            .snippet(m_fromLocation.getProvider())
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.map)));
                }
                if (m_toLocation != null) {
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(m_toLocation.getLatitude(), m_toLocation.getLongitude()))
                            .title(getResources().getString(R.string.location_activity_to_text))
                            .snippet(m_toLocation.getProvider())
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.map)));
                }
            }
        });
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().findViewById(R.id.location_activity_to_where).getWindowToken(), 0);
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(LocationChooserActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(LocationChooserActivity.this,
                        Manifest.permission.READ_CONTACTS)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(LocationChooserActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                (com.google.android.gms.location.LocationListener) this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void notify(String str, String tag) {
        if(str==null||tag==null) return;
        if(tag.equals(getResources().getString(R.string.location_from_where_tag).toString())){
            m_fromEditText.setText(str);
            chooseFirstOption(m_fromEditText,m_fromListView,true);
        } else if(tag.equals(getResources().getString(R.string.location_to_where_tag).toString())){
            m_toEditText.setText(str);
            chooseFirstOption(m_toEditText, m_toListView, false);
        }
    }

    private void chooseFirstOption(EditText editText, ListView listview, Boolean isFrom) {
        try {
            Thread.sleep(1000);
            if(listview!=null && listview.getAdapter().getCount()>=1){
                String closestResult = (String) listview.getAdapter().getItem(0);
                editText.setText(closestResult);
                if(isFrom) {
                    m_fromLocation= getLocation(closestResult);
                    diveToLocation(m_fromLocation);
                } else {
                    m_toLocation = getLocation(closestResult);
                    diveToLocation(m_toLocation);
                }
                listview.setVisibility(View.GONE);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
