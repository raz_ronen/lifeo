package com.example.raz.lifeo.Components;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.raz.lifeo.R;

/**
 * Created by Raz on 7/18/2017.
 */
public class NewEventPickHowPopUp extends DialogFragment {

    private TextView m_walk = null;
    private TextView m_car = null;
    private TextView m_bike = null;
    private TextView m_bus = null;
    private TextView m_train = null;
    private TextView m_whereTextView = null;

    public void setHowTextView(TextView whereTextView){
        this.m_whereTextView = whereTextView;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.new_event_how_popup, null);

        m_walk = (TextView) v.findViewById(R.id.new_event_how_walk);
        m_car = (TextView) v.findViewById(R.id.new_event_how_car);
        m_bike = (TextView) v.findViewById(R.id.new_event_how_bike);
        m_bus = (TextView) v.findViewById(R.id.new_event_how_bus);
        m_train = (TextView) v.findViewById(R.id.new_event_how_train);

        SetAllTextViewColors();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_whereTextView.setText(((TextView) v).getText().toString());
                dismiss();
            }
        };
        m_walk.setOnClickListener(listener);
        m_car.setOnClickListener(listener);
        m_bike.setOnClickListener(listener);
        m_bus.setOnClickListener(listener);
        m_train.setOnClickListener(listener);

        return new AlertDialog.Builder(getActivity()).setView(v).create();
    }

    private void SetAllTextViewColors() {
        if(m_whereTextView==null || m_walk==null ||  m_car==null
                || m_bike ==null || m_bus==null || m_train==null) return;

        m_walk.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_car.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_bike.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_bus.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_train.setTextColor(getResources().getColor(R.color.greyish_brown));

        if(m_whereTextView.getText().toString().equals(m_walk.getText().toString())){
            m_walk.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_whereTextView.getText().toString().equals(m_car.getText().toString())){
            m_car.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_whereTextView.getText().toString().equals(m_bike.getText().toString())){
            m_bike.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_whereTextView.getText().toString().equals(m_bus.getText().toString())){
            m_bus.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_whereTextView.getText().toString().equals(m_train.getText().toString())){
            m_train.setTextColor(getResources().getColor(R.color.greeny_blue));
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

}