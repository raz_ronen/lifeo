package com.example.raz.lifeo.Entities;

import android.location.Location;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Raz on 7/18/2017.
 */
public class L_Event implements Serializable{
    private String m_date = null;
    private String m_title = null;
    private String m_how = null;

    public String getM_calculatedGoToBed() {
        return m_calculatedGoToBed;
    }

    public void setM_calculatedGoToBed(String m_calculatedGoToBed) {
        this.m_calculatedGoToBed = m_calculatedGoToBed;
    }

    public String getM_calculatedWwakeUp() {
        return m_calculatedWwakeUp;
    }

    public void setM_calculatedWwakeUp(String m_calculatedWwakeUp) {
        this.m_calculatedWwakeUp = m_calculatedWwakeUp;
    }

    public String getM_calculatedHitRoad() {
        return m_calculatedHitRoad;
    }

    public void setM_calculatedHitRoad(String m_calculatedHitRoad) {
        this.m_calculatedHitRoad = m_calculatedHitRoad;
    }

    public String getM_calculatedArrival() {
        return m_calculatedArrival;
    }

    public void setM_calculatedArrival(String m_calculatedArrival) {
        this.m_calculatedArrival = m_calculatedArrival;
    }

    private String m_calculatedGoToBed = null;
    private String m_calculatedWwakeUp = null;
    private String m_calculatedHitRoad = null;
    private String m_calculatedArrival = null;

    public Integer getDayOfMonth(){
        return Integer.parseInt(m_date.substring(0,m_date.indexOf("/")));
    }
    public Integer getMonth(){
        return Integer.parseInt(m_date.substring(m_date.indexOf("/")+1,m_date.lastIndexOf("/")));
    }
    public Integer getYear(){
        return Integer.parseInt(m_date.substring(m_date.lastIndexOf("/")+1,m_date.indexOf(" ")));
    }
    public String getDay(){
        return m_date.substring(m_date.indexOf(" ") + 1, m_date.lastIndexOf(" "));
    }
    public Integer getHour(){
        return Integer.parseInt(m_date.substring(m_date.indexOf(":") - 2, m_date.indexOf(":")));
    }
    public Integer getMin(){
        return Integer.parseInt(m_date.substring(m_date.indexOf(":")+1));
    }

    public String getHow() {
        return m_how;
    }

    public void setHow(String m_how) {
        this.m_how = m_how;
    }

    private Integer m_sleep = null;
    private Integer m_wakeUp = null;
    private Integer m_arrival = null;

    private L_Location m_locationFrom = null;

    public L_Location getLocationFrom() {
        return m_locationFrom;
    }

    public void setLocationFrom(L_Location m_locationFrom) {
        this.m_locationFrom = m_locationFrom;
    }

    public L_Location getLocationTo() {
        return m_locationTo;
    }

    public void setLocationTo(L_Location m_locationTo) {
        this.m_locationTo = m_locationTo;
    }

    private L_Location m_locationTo = null;

    public Integer getSleep() {
        return m_sleep;
    }

    public void setSleep(Integer m_sleep) {
        this.m_sleep = m_sleep;
    }

    public Integer getWakeUp() {
        return m_wakeUp;
    }

    public void setWakeUp(Integer m_wakeUp) {
        this.m_wakeUp = m_wakeUp;
    }

    public Integer getArrival() {
        return m_arrival;
    }

    public void setArrival(Integer m_arrival) {
        this.m_arrival = m_arrival;
    }

    public Integer getAvg() {
        if(m_wakeUp==null || m_arrival==null || m_sleep==null) return 0;
        return (m_wakeUp + m_arrival + m_sleep) / 3;
    }

    public L_Event(String m_date, String m_title, String how, L_Location from, L_Location to) {
        this.m_date = m_date;
        this.m_title = m_title;
        this.m_how = how;
        this.m_locationFrom = from;
        this.m_locationTo = to;
    }

    public String getDate() {
        return m_date;
    }

    public void setDate(String m_date) {
        this.m_date = m_date;
    }

    public String getTitle() {
        return m_title;
    }

    public void setTitle(String m_title) {
        this.m_title = m_title;
    }

}
