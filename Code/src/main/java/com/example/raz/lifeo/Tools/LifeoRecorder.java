package com.example.raz.lifeo.Tools;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

import com.example.raz.lifeo.Interfaces.Notified;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Raz on 7/20/2017.
 */
public class LifeoRecorder implements RecognitionListener {

    private static final String TAG = "LifeoRecorder";
    private String m_tag = null;
    private Activity m_activity = null;
    private Notified m_notified = null;
    private SpeechRecognizer m_speechRecognizer = null;
    private Boolean authorized = false;
    private List<String> supportedLanguages;

    public LifeoRecorder(Activity m_activity, Notified notified, String tag) {
        this.m_activity = m_activity;
        this.m_notified = notified;
        this.m_tag = tag;

        Intent detailsIntent =  new Intent(RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);
        m_activity.sendOrderedBroadcast(
                detailsIntent, null, new LanguageDetailsChecker(), null, Activity.RESULT_OK, null, null);
        getPremissionIfNeeded();
        m_speechRecognizer = SpeechRecognizer.createSpeechRecognizer(m_activity);
        m_speechRecognizer.setRecognitionListener(this);
    }

    private void getPremissionIfNeeded() {
        int retrys = 3;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String requiredPermission = Manifest.permission.RECORD_AUDIO;

            // If the user previously denied this permission then show a message explaining why
            // this permission is needed
            while(m_activity.checkCallingOrSelfPermission(requiredPermission) == PackageManager.PERMISSION_DENIED && retrys>=3) {
                try {
                    m_activity.requestPermissions(new String[]{requiredPermission}, 101);
                    Thread.sleep(1000);
                    --retrys;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            authorized = true;
        }
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.d(TAG, "onReadyForSpeech");
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.d(TAG, "Speech starting");
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.d(TAG, "onRmsChanged");
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.d(TAG, "onBufferReceived");
    }

    @Override
    public void onEndOfSpeech() {
        Log.d(TAG, "onEndofSpeech");
    }

    @Override
    public void onError(int error) { Log.d(TAG, "Error listening for speech: " + error);    }

    @Override
    public void onResults(Bundle results) {
        String str = new String();
        Log.d(TAG, "onResults " + results);
        ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        for (int i = 0; i < data.size(); i++)
        {
            Log.d(TAG, "result " + data.get(i));
            str += data.get(i);
        }
        if(data.size()>=1){
            m_notified.notify(data.get(0), m_tag);
        }
    }

    @Override
    public void onPartialResults(Bundle partialResults) {

    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }

    public void startRecording(){
        if(authorized) {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            //list of all available languages in device
//            if(supportedLanguages!=null){
//                for(String lang: supportedLanguages){
//                    Log.d("LANGUAGE",lang);
//                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, lang);
//                }
//            }
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "he-IL");
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, m_activity.getPackageName());
            Log.d(TAG, "Start recording..");
            Toast.makeText(m_activity,"Recording..",Toast.LENGTH_SHORT).show();
            m_speechRecognizer.startListening(intent);
        }
    }

    public void stopRecording(){
        if(authorized){
            Log.d(TAG,"Stopping recording..");
            Toast.makeText(m_activity,"Analysing..",Toast.LENGTH_LONG).show();
            m_speechRecognizer.stopListening();
        }
    }

    public class LanguageDetailsChecker extends BroadcastReceiver
    {

        private String languagePreference;

        @Override
        public void onReceive(Context context, Intent intent)
        {
            Bundle results = getResultExtras(true);
            if (results.containsKey(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE))
            {
                languagePreference =
                        results.getString(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE);
            }
            if (results.containsKey(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES))
            {
                supportedLanguages =
                        results.getStringArrayList(
                                RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES);
            }
        }
    }
}
