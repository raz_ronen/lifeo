package com.example.raz.lifeo.Components;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.raz.lifeo.Entities.EventsContainer;
import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.R;
import com.example.raz.lifeo.Tools.DayOfWeek;
import com.google.gson.Gson;

import java.util.GregorianCalendar;

/**
 * Created by Raz on 7/18/2017.
 */
public class NewEventPickTimePopUp extends DialogFragment {

    private TextView m_timeTextView = null;
    private DatePicker m_datePicker = null;
    private TimePicker m_timePicker = null;
    private ImageButton m_button = null;
    private String m_pickedTime = null;
    private Activity m_act;
    private L_Event m_event = null;

    public void setTimeTextView(Activity act, TextView timeTextView, int eventNum){
        this.m_act = act;
        this.m_timeTextView = timeTextView;
        if(eventNum!=-1){
            populateDataByEvent(eventNum);
        }
    }

    private void populateDataByEvent(int eventNum) {
        SharedPreferences mPrefs = m_act.getPreferences(m_act.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(m_act.getResources().getString(R.string.events_shared_prefences_tag),
                "");
        EventsContainer obj = gson.fromJson(json, EventsContainer.class);
        this.m_event = obj.getEvents().get(eventNum);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.new_event_time_popup, null);

        m_datePicker = (DatePicker) v.findViewById(R.id.new_event_time_date_picker);
        m_timePicker = (TimePicker) v.findViewById(R.id.new_event_time_time_picker);
        m_button = (ImageButton) v.findViewById(R.id.new_event_time_popup_button);

        m_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = m_datePicker.getYear();
                int month = m_datePicker.getMonth()+1;
                int dayInMonth = m_datePicker.getDayOfMonth();
                GregorianCalendar gregorianCalendar = new GregorianCalendar(year, month-1, dayInMonth);
                int dayOfWeek = gregorianCalendar.get(gregorianCalendar.DAY_OF_WEEK);
                String date = String.valueOf(dayInMonth) + "/"
                        + String.valueOf(month) + "/"
                        + String.valueOf(year) + " "
                        + DayOfWeek.Day(getActivity(),dayOfWeek-1);
                String mins = (m_timePicker.getCurrentMinute()<10)?"0"+String.valueOf(m_timePicker.getCurrentMinute()):
                        String.valueOf(m_timePicker.getCurrentMinute());
                String hours = (m_timePicker.getCurrentHour()<10)?"0"+String.valueOf(m_timePicker.getCurrentHour()):
                        String.valueOf(m_timePicker.getCurrentHour());
                String time = hours + ":" + mins;
                m_pickedTime = date + " " + time;
                Log.d("Chosen time", m_pickedTime);
                dismiss();

            }
        });
        setTime();
        return new AlertDialog.Builder(getActivity()).setView(v).create();
    }

    private void setTime() {
        if(m_event!=null) {
            m_datePicker.updateDate(m_event.getYear(), m_event.getMonth() - 1, m_event.getDayOfMonth());
            m_timePicker.setCurrentHour(m_event.getHour());
            m_timePicker.setCurrentMinute(m_event.getMin());
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(m_pickedTime!=null && m_timeTextView!=null) m_timeTextView.setText(m_pickedTime);
        super.onDismiss(dialog);
    }

}
