package com.example.raz.lifeo.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.raz.lifeo.Components.NewEventPickHowPopUp;
import com.example.raz.lifeo.Components.NewEventPickTimePopUp;
import com.example.raz.lifeo.Components.TellUsPopUp;
import com.example.raz.lifeo.Entities.EventsContainer;
import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.Entities.L_Location;
import com.example.raz.lifeo.Interfaces.Notified;
import com.example.raz.lifeo.R;
import com.example.raz.lifeo.Tools.DayOfWeek;
import com.example.raz.lifeo.Tools.LifeoRecorder;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class NewEventActivity extends LifeoActionBarActivity implements Notified {

    private static final int GET_EVENT_LOCATION = 1;
    private EditText m_nameEditText = null;
    private TextView m_timeTextView = null;
    private TextView m_whereTextView = null;
    private TextView m_howTextView = null;
    private RelativeLayout m_nameSelection = null;
    private RelativeLayout m_timeSelection = null;
    private RelativeLayout m_whereSelection = null;
    private RelativeLayout m_howSelection = null;
    private L_Location m_fromWhere = null;
    private L_Location m_toWhere = null;
    private LifeoRecorder m_nameRecorder = null;
    private ImageButton m_createEventButton = null;
    private int m_eventNum = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        InitUI();
        CheckForIntent();
        SetRecorder();
    }

    private void SetRecorder() {
        m_nameRecorder = new LifeoRecorder(this, this, getResources().getString(R.string.new_event_name_tag));
        final ImageButton recorder = (ImageButton) findViewById(R.id.new_event_name_recorder);
        recorder.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    m_nameRecorder.startRecording();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    m_nameRecorder.stopRecording();
                }
                return true;
            }
        });
    }

    private void CheckForIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            L_Event event = (L_Event) intent.getSerializableExtra(getResources().getString(R.string.tags_event));
            L_Location from = (L_Location) intent.getSerializableExtra(getResources().getString(R.string.tags_event_from));
            L_Location to = (L_Location) intent.getSerializableExtra(getResources().getString(R.string.tags_event_to));
            if (event == null) return;
            if (event.getTitle() != null) m_nameEditText.setText(event.getTitle());
            if (event.getDate() != null)
                m_timeTextView.setText(event.getDate());
            if (event.getHow() != null) m_howTextView.setText(event.getHow());
            if (to != null) {
                m_whereTextView.setText(to.getProvider());
                m_toWhere = to;
            }
            if (from != null) {
                m_fromWhere = from;
            }
            if(intent.hasExtra(getResources().getString(R.string.event_num_tag))){
                int num = intent.getIntExtra(getResources().getString(R.string.event_num_tag),-1);
                if(num==-1) return;
                else m_eventNum = num;
            }
            ((TextView) findViewById(R.id.new_event_title)).setText(getResources().getString(R.string.edit_event_title).toString());
        }
    }

    private void InitUI() {
        m_nameEditText = (EditText) findViewById(R.id.new_event_name_edit_text);
        m_timeTextView = (TextView) findViewById(R.id.new_event_time_text_view);
        m_whereTextView = (TextView) findViewById(R.id.new_event_where_text_view);
        m_howTextView = (TextView) findViewById(R.id.new_event_how_text_view);
        m_nameSelection = (RelativeLayout) findViewById(R.id.new_event_name_selection);
        m_timeSelection = (RelativeLayout) findViewById(R.id.new_event_time_selection);
        m_whereSelection = (RelativeLayout) findViewById(R.id.new_event_where_selection);
        m_howSelection = (RelativeLayout) findViewById(R.id.new_event_how_selection);
        m_createEventButton = (ImageButton) findViewById(R.id.create_event_btn);

        m_nameSelection.setOnClickListener(nameListener);
        m_timeSelection.setOnClickListener(timeListener);
        m_whereSelection.setOnClickListener(whereListener);
        m_howSelection.setOnClickListener(howListener);
        m_createEventButton.setOnClickListener(createButtonListener);

        setDefaultTime();
    }

    private void setDefaultTime() {
        if (m_timeTextView == null) return;
        String date = String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) + "/"
                + String.valueOf(1 + Calendar.getInstance().get(Calendar.MONTH)) + "/"
                + String.valueOf(Calendar.getInstance().getTime().getYear() + 1900) + " "
                + DayOfWeek.Day(this, Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1);
        String hours = String.valueOf(Calendar.getInstance().getTime().getHours());
        String mins = String.valueOf(Calendar.getInstance().getTime().getMinutes());
        hours = (hours.length() == 1) ? "0" + hours : hours;
        mins = (mins.length() == 1) ? "0" + mins : mins;
        String time = hours + ":" + mins;
        String m_pickedTime = date + " " + time;
        m_timeTextView.setText(m_pickedTime);
    }

    private View.OnClickListener nameListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (m_nameEditText != null) m_nameEditText.requestFocus();
        }
    };

    private View.OnClickListener timeListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (m_timeTextView == null) return;
            NewEventPickTimePopUp timePopUp = new NewEventPickTimePopUp();
            timePopUp.setTimeTextView(NewEventActivity.this,m_timeTextView,m_eventNum);
            timePopUp.show(getFragmentManager(), getResources().getString(R.string.default_tag));
        }
    };

    private View.OnClickListener howListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (m_howTextView == null) return;
            NewEventPickHowPopUp howPopUp = new NewEventPickHowPopUp();
            howPopUp.setHowTextView(m_howTextView);
            howPopUp.show(getFragmentManager(), getResources().getString(R.string.default_tag));
        }
    };

    private View.OnClickListener whereListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(NewEventActivity.this, LocationChooserActivity.class);
            if(m_fromWhere!=null){
                intent.putExtra(getResources().getString(R.string.tags_event_from), m_fromWhere);
            }
            if(m_toWhere!=null){
                intent.putExtra(getResources().getString(R.string.tags_event_to), m_toWhere);
            }
            startActivityForResult(intent, GET_EVENT_LOCATION);
        }
    };

    private View.OnClickListener createButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            L_Event event = new L_Event(m_timeTextView.getText().toString(),
                    m_nameEditText.getText().toString(),
                    m_howTextView.getText().toString(),
                    m_fromWhere,
                    m_toWhere);

            setFakeEvent(event);
            AdjustEvents(event);
            startActivity(new Intent(NewEventActivity.this,HomeActivity.class));
        }
    };

    private void setFakeEvent(L_Event event) {
        int rand1 = (int) (Math.random() * 100 + 1);
        int rand2 = (int) (Math.random() * 100 + 1);
        int rand3 = (int) (Math.random() * 100 + 1);
        event.setSleep(rand1);
        event.setArrival(rand2);
        event.setWakeUp(rand3);

        int goToBedM = (int) (Math.random() * 59 + 1);
        int arrivalM = (int) (Math.random() * 59 + 1);
        int wakeupH = 6 + (int) (Math.random() * 2 + 1);
        int wakeupM = (int) (Math.random() * 59 + 1);
        int hitRoadH = 6 + (int) (Math.random() * 2 + 1);
        int hitRoadM = (int) (Math.random() * 59 + 1);
        String wakeUp;
        String hitRoad;
        String goToBed = String.valueOf(20 + (int) (Math.random() * 3 + 1)) + ":" + ((goToBedM<10)?"0"+ String.valueOf(goToBedM):goToBedM);
        if(wakeupH>hitRoadH || (wakeupH==hitRoadH && wakeupM > hitRoadM)){
            wakeUp = "0" + String.valueOf(hitRoadH) + ":" + ((hitRoadM<10)?"0"+ String.valueOf(hitRoadM):hitRoadM);
            hitRoad = "0" + String.valueOf(wakeupH) + ":" + ((wakeupM<10)?"0"+ String.valueOf(wakeupM):wakeupM);
        } else {
            wakeUp = "0" + String.valueOf(wakeupH) + ":" + ((wakeupM<10)?"0"+ String.valueOf(wakeupM):wakeupM);
            hitRoad = "0" + String.valueOf(hitRoadH) + ":" + ((hitRoadM<10)?"0"+ String.valueOf(hitRoadM):hitRoadM);
        }
        String arrival = "0" + String.valueOf(8 + (int) (Math.random() * 1 + 1)) + ":" + ((arrivalM<10)?"0"+ String.valueOf(arrivalM):arrivalM);

        event.setM_calculatedGoToBed(goToBed);
        event.setM_calculatedWwakeUp(wakeUp);
        event.setM_calculatedHitRoad(hitRoad);
        event.setM_calculatedArrival(arrival);
    }

    private void AdjustEvents(L_Event event) {
        SharedPreferences mPrefs = getSharedPreferences(getResources().getString(R.string.default_tag), Context.MODE_MULTI_PROCESS);
        Gson gson = new Gson();
        String json = mPrefs.getString(getResources().getString(R.string.events_shared_prefences_tag),
                getResources().getString(R.string.empty).toString());
        if(json.equals(getResources().getString(R.string.empty).toString())) return;
        EventsContainer container = gson.fromJson(json, EventsContainer.class);
        if(m_eventNum==-1){
            container.getEvents().add(event);
        } else {
            container.getEvents().remove(m_eventNum);
            container.getEvents().add(m_eventNum,event);
        }
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        json = gson.toJson(container);
        prefsEditor.putString(getResources().getString(R.string.events_shared_prefences_tag), json);
        prefsEditor.apply();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_EVENT_LOCATION && resultCode == RESULT_OK && data != null) {
            if (data.hasExtra(getResources().getString(R.string.location_from_where_tag))) {
                m_fromWhere = (L_Location) data.getSerializableExtra(getResources().getString(R.string.location_from_where_tag));
            }
            if (data.hasExtra(getResources().getString(R.string.location_to_where_tag))) {
                m_toWhere = (L_Location) data.getSerializableExtra(getResources().getString(R.string.location_to_where_tag));
            }
            if (m_fromWhere != null) Log.d("FROM", m_fromWhere.getProvider());
            if (m_toWhere != null) {
                Log.d("TAG", m_toWhere.getProvider());
                m_whereTextView.setText(m_toWhere.getProvider());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void notify(String str, String tag) {
        if (str == null || tag == null) return;
        if (tag.equals(getResources().getString(R.string.new_event_name_tag).toString())) {
            m_nameEditText.setText(str);
        }
    }
}
