package com.example.raz.lifeo.Fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.raz.lifeo.Activities.HomeActivity;
import com.example.raz.lifeo.Activities.LifeoActionBarActivity;
import com.example.raz.lifeo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LifeoActionBar extends Fragment {

    private static final int DEFAULT_FADE_TIME = 1000;
    private static final double SCREEN_CUT = 0.7;
    private static final int OPTIONS_NUM = 6;
    private static Integer MENU_HEIGHT = 0;
    private static Integer WIDTH = 0;

    private static final String TAG = "LifeoActionBar.java";
    private LinearLayout m_line1 = null;
    private LinearLayout m_line2 = null;
    private LinearLayout m_line3 = null;
    private LinearLayout m_line2Clone = null;
    private LinearLayout m_settingsBtn = null;
    private LinearLayout m_actionBarFrame = null;
    private LinearLayout m_settingsBtnClone = null;
    private LinearLayout m_actionBarFrameClone = null;
    private LinearLayout m_mainFrame = null;
    private LinearLayout m_subMenu = null;
    private RelativeLayout m_display = null;
    private RelativeLayout m_sideShadow = null;
    private Menu m_menuFragment = null;
    private Boolean open = false;
    private Boolean inProgress = false;

    private Animation m_openSubmenAnimation;
    private Animation m_closeSubmenAnimation;
    private Animation m_moveOutContentFrame;
    private Animation m_moveInContentFrame;
    private Animation m_btnRotateOpen1;
    private Animation m_btnRotateOpen2;
    private Animation m_btnRotateClose1;
    private Animation m_btnRotateClose2;

    public LifeoActionBar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lifeo_action_bar, container, false);

        initUI(v);
        setDefaultHeight();
        defineAnimation();
        return v;
    }

    private void defineAnimation() {
        m_openSubmenAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.open_submenu);
        m_closeSubmenAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.close_submenu);
        m_moveOutContentFrame = AnimationUtils.loadAnimation(getActivity(), R.anim.close_content_screen);
        m_moveInContentFrame = AnimationUtils.loadAnimation(getActivity(), R.anim.open_content_screen);
        m_btnRotateOpen1 = AnimationUtils.loadAnimation(getActivity(), R.anim.menu_btn_rotate_open1);
        m_btnRotateOpen2 = AnimationUtils.loadAnimation(getActivity(), R.anim.menu_btn_rotate_open2);
        m_btnRotateClose1 = AnimationUtils.loadAnimation(getActivity(), R.anim.menu_btn_rotate_close1);
        m_btnRotateClose2 = AnimationUtils.loadAnimation(getActivity(), R.anim.menu_btn_rotate_close2);

        m_openSubmenAnimation.setAnimationListener(m_openSubmenAnimationListener);
        m_closeSubmenAnimation.setAnimationListener(m_closeSubmenAnimationListener);
        m_moveOutContentFrame.setAnimationListener(m_moveOutContentFrameListener);
        m_moveInContentFrame.setAnimationListener(m_moveInContentFrameListener);
    }

    private void setDefaultHeight() {
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        WIDTH = size.x;
        MENU_HEIGHT = size.y;
    }

    public void setContainers(LinearLayout submenu, LinearLayout mainFrame, Menu menuFragment,
                              RelativeLayout backgroundDisplay, RelativeLayout display) {
        this.m_mainFrame = mainFrame;
        this.m_subMenu = submenu;
        this.m_menuFragment = menuFragment;
        this.m_sideShadow = backgroundDisplay;
        this.m_display = display;

        m_sideShadow.bringToFront();
        m_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(open && !inProgress) closeSubWindow();
            }
        });
    }

    private void initUI(View v) {
        m_settingsBtn = (LinearLayout) v.findViewById(R.id.lifeo_action_bar__btn);
        m_actionBarFrame = (LinearLayout) v.findViewById(R.id.lifeo_action_bar_frame);
        m_line1 = (LinearLayout) v.findViewById(R.id.lifeo_action_bar_line_1);
        m_line2 = (LinearLayout) v.findViewById(R.id.lifeo_action_bar_line_2);
        m_line3 = (LinearLayout) v.findViewById(R.id.lifeo_action_bar_line_3);
        m_line2Clone = (LinearLayout) v.findViewById(R.id.lifeo_action_bar_line_2_clone);

        m_settingsBtnClone = (LinearLayout) v.findViewById(R.id.lifeo_action_bar__btn_clone);
        m_actionBarFrameClone = (LinearLayout) v.findViewById(R.id.lifeo_action_bar_frame_clone);

        ((ImageView) v.findViewById(R.id.lifeo_action_bar_logo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HomeActivity.class));
            }
        });

        m_settingsBtn.setOnClickListener(onClickListener);
        m_settingsBtnClone.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (inProgress) return;
            if (open) closeSubWindow();
            else openSubWindow();
        }
    };

    private void openSubWindow() {
        inProgress = true;
        m_menuFragment.fadeIn();
        fadeFrame(false, DEFAULT_FADE_TIME, m_line1);
        fadeFrame(false, DEFAULT_FADE_TIME, m_line3);
        fadeFrame(true, DEFAULT_FADE_TIME, m_sideShadow);
        m_subMenu.startAnimation(m_openSubmenAnimation);
        m_mainFrame.startAnimation(m_moveOutContentFrame);
        m_line2.startAnimation(m_btnRotateOpen1);
        m_line2Clone.startAnimation(m_btnRotateOpen2);
        m_mainFrame.bringToFront();
    }

    private void closeSubWindow() {
        inProgress = true;
        m_menuFragment.fadeOut();
        fadeFrame(true, DEFAULT_FADE_TIME, m_line1);
        fadeFrame(true, DEFAULT_FADE_TIME, m_line3);
        fadeFrame(false, DEFAULT_FADE_TIME, m_sideShadow);
        fadeFrame(false, DEFAULT_FADE_TIME, m_subMenu);
        m_closeSubmenAnimation.setFillAfter(true);
        m_subMenu.startAnimation(m_closeSubmenAnimation);
        m_mainFrame.startAnimation(m_moveInContentFrame);
        m_line2.startAnimation(m_btnRotateClose1);
        m_line2Clone.startAnimation(m_btnRotateClose2);
    }

    private void fadeFrame(final Boolean appear, final int time, final ViewGroup layout) {
        Animation fade = (appear) ? new AlphaAnimation(0, 1) : new AlphaAnimation(1, 0);
        fade.setInterpolator(new AccelerateInterpolator());
        fade.setDuration(time);
        AnimationSet animation = new AnimationSet(false); //change to false
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (appear) layout.setVisibility(View.VISIBLE);
                else layout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                animation.reset();
            }
        });
        animation.addAnimation(fade);
        layout.startAnimation(animation);
    }

    private Animation.AnimationListener m_closeSubmenAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            lp.setMargins(-(int)(m_subMenu.getWidth()),0,(int)(m_subMenu.getWidth()),0);
            m_subMenu.setLayoutParams(lp);
            inProgress = false;
            open = false;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };
    private Animation.AnimationListener m_moveOutContentFrameListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            lp.setMarginStart((int) (SCREEN_CUT * m_mainFrame.getWidth()));
            lp.setMarginEnd(-(int) (SCREEN_CUT * m_mainFrame.getWidth()));
            m_mainFrame.setLayoutParams(lp);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };
    private Animation.AnimationListener m_moveInContentFrameListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            m_mainFrame.setLayoutParams(lp);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };
    private Animation.AnimationListener m_openSubmenAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            lp.setMargins(-(int)(m_subMenu.getWidth()),0,(int)(m_subMenu.getWidth()),0);
            m_subMenu.setLayoutParams(lp);
            m_subMenu.setVisibility(View.VISIBLE);
            inProgress = true;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    lp.setMargins(-(int)(0.3*m_subMenu.getWidth()),0,(int)(0.3*m_subMenu.getWidth()),0);
                    m_subMenu.setLayoutParams(lp);
                }
            });
            inProgress = false;
            open = true;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };
}