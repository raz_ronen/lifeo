package com.example.raz.lifeo.Activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toolbar;

import com.example.raz.lifeo.Entities.EventsContainer;
import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.Entities.L_Location;
import com.example.raz.lifeo.Fragments.LifeoActionBar;
import com.example.raz.lifeo.Fragments.Menu;
import com.example.raz.lifeo.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class LifeoActionBarActivity extends Activity {

    protected RelativeLayout fullLayout;
    protected FrameLayout subActivityContent;
    private Fragment m_actionBar = null;
    private Fragment m_subMeuFragment = null;
    private LinearLayout m_submenu = null;
    private LinearLayout m_mainFrame = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createFakeEvents();
    }

    protected  void createFakeEvents(){
        SharedPreferences mPrefs = getSharedPreferences(getResources().getString(R.string.default_tag), Context.MODE_MULTI_PROCESS);
        Gson gson = new Gson();

        String data = mPrefs.getString(getResources().getString(R.string.events_shared_prefences_tag).toString(),
                getResources().getString(R.string.empty));
        if(data.equals(getResources().getString(R.string.empty).toString())){
            List<L_Event> myEvents = new ArrayList<>();
            L_Location from = new L_Location("Tel Aviv");
            L_Location to = new L_Location("Ramat Hayal");
            from.setLatitude(35);
            from.setLongitude(35);
            to.setLatitude(35);
            to.setLongitude(35);

            myEvents.add(new L_Event("29/07/2017 Saturday 07:00", "Moving", "Car", from, to));
            myEvents.add(new L_Event("24/07/2017 Saturday 18:10", "Wedding", "Car", from, to));
            myEvents.add(new L_Event("21/07/2017 Saturday 19:40", "Giants match", "Car", from, to));
            myEvents.add(new L_Event("16/07/2017 Saturday 08:20", "Recipe to try", "Car", from, to));
            myEvents.add(new L_Event("12/07/2017 Saturday 15:40", "Birthday Gift", "Car", from, to));
            myEvents.add(new L_Event("08/07/2017 Saturday 14:30", "Going to the Movies", "Car", from, to));
            myEvents.add(new L_Event("04/07/2017 Saturday 09:25", "Summer BBQ", "Car", from, to));
            myEvents.add(new L_Event("03/07/2017 Saturday 19:00", "Basketball Game", "Car", from, to));
            myEvents.add(new L_Event("28/06/2017 Saturday 10:16", "Brunch this weekend", "Car", from, to));
            myEvents.add(new L_Event("28/06/2017 Saturday 22:16", "Adir Miller StandUp", "Car", from, to));
            myEvents.add(new L_Event("23/06/2017 Saturday 13:30", "Crossfit", "Car", from, to));
            myEvents.add(new L_Event("21/06/2017 Saturday 12:00", "Lnuch with Or", "Car", from, to));
            myEvents.add(new L_Event("12/06/2017 Saturday 16:00", "Shopping", "Car", from, to));
            myEvents.add(new L_Event("7/06/2017 Saturday 21:20", "Date", "Car", from, to));
            myEvents.add(new L_Event("3/06/2017 Saturday 14:00", "Conference Meeting", "Car", from, to));
            myEvents.add(new L_Event("2/06/2017 Saturday 06:00", "Bicycle ride", "Car", from, to));

            for(L_Event event: myEvents){
                int rand1 = (int) (Math.random() * 100 + 1);
                int rand2 = (int) (Math.random() * 100 + 1);
                int rand3 = (int) (Math.random() * 100 + 1);
                event.setSleep(rand1);
                event.setArrival(rand2);
                event.setWakeUp(rand3);

                int goToBedM = (int) (Math.random() * 59 + 1);
                int arrivalM = (int) (Math.random() * 59 + 1);
                int wakeupH = 6 + (int) (Math.random() * 2 + 1);
                int wakeupM = (int) (Math.random() * 59 + 1);
                int hitRoadH = 6 + (int) (Math.random() * 2 + 1);
                int hitRoadM = (int) (Math.random() * 59 + 1);
                String wakeUp;
                String hitRoad;
                String goToBed = String.valueOf(20 + (int) (Math.random() * 3 + 1)) + ":" + ((goToBedM<10)?"0"+ String.valueOf(goToBedM):goToBedM);
                if(wakeupH>hitRoadH || (wakeupH==hitRoadH && wakeupM > hitRoadM)){
                    wakeUp = "0" + String.valueOf(hitRoadH) + ":" + ((hitRoadM<10)?"0"+ String.valueOf(hitRoadM):hitRoadM);
                    hitRoad = "0" + String.valueOf(wakeupH) + ":" + ((wakeupM<10)?"0"+ String.valueOf(wakeupM):wakeupM);
                } else {
                    wakeUp = "0" + String.valueOf(wakeupH) + ":" + ((wakeupM<10)?"0"+ String.valueOf(wakeupM):wakeupM);
                    hitRoad = "0" + String.valueOf(hitRoadH) + ":" + ((hitRoadM<10)?"0"+ String.valueOf(hitRoadM):hitRoadM);
                }
                String arrival = "0" + String.valueOf(8 + (int) (Math.random() * 1 + 1)) + ":" + ((arrivalM<10)?"0"+ String.valueOf(arrivalM):arrivalM);

                event.setM_calculatedGoToBed(goToBed);
                event.setM_calculatedWwakeUp(wakeUp);
                event.setM_calculatedHitRoad(hitRoad);
                event.setM_calculatedArrival(arrival);
            }

            EventsContainer container = new EventsContainer(myEvents);
            String content = gson.toJson(container);
            SharedPreferences.Editor prefsEditor = mPrefs.edit();
            prefsEditor.putString(getResources().getString(R.string.events_shared_prefences_tag),content);
            prefsEditor.commit();
        }

    }

    @Override
    public void setContentView(int layoutResID) {
        fullLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.activity_lifeo_action_bar_activity, null);  // The base layout
        subActivityContent = (FrameLayout) fullLayout.findViewById(R.id.content_frame);            // The frame layout where the activity content is placed.
        getLayoutInflater().inflate(layoutResID, subActivityContent, true);            // Places the activity layout inside the activity content frame.
        initUI();
        super.setContentView(fullLayout);                                                       // Sets the content view as the merged layouts.
    }

    protected void initUI(){
        m_actionBar = (Fragment) getFragmentManager().findFragmentById(R.id.home_lifeo_action_bar);
        m_submenu = (LinearLayout) fullLayout.findViewById(R.id.home_submenu);
        m_mainFrame = (LinearLayout) fullLayout.findViewById(R.id.home_main_frame);
        m_subMeuFragment = (Fragment) getFragmentManager().findFragmentById(R.id.home_lifeo_submenu);

        if (m_actionBar instanceof LifeoActionBar && m_subMeuFragment instanceof Menu) {
            ((LifeoActionBar) m_actionBar).setContainers(m_submenu,m_mainFrame,((Menu) m_subMeuFragment),
                    (RelativeLayout) fullLayout.findViewById(R.id.action_bar_background),
                    (RelativeLayout) fullLayout.findViewById(R.id.home_display));
        }

        Log.d("Language", Locale.getDefault().getDisplayLanguage());
    }

}
