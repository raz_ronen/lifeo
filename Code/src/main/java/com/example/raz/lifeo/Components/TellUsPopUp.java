package com.example.raz.lifeo.Components;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.raz.lifeo.R;

/**
 * Created by Raz on 7/17/2017.
 */
public class TellUsPopUp extends DialogFragment {


    private TextView m_chronicallyLate = null;
    private TextView m_usuallyLate = null;
    private TextView m_sometimesLate = null;
    private TextView m_usuallyOnTime = null;
    private TextView m_neverLate = null;
    private TextView m_tellUsTextView = null;

    public void setTellUsTextView(TextView tellUsTextView){
        this.m_tellUsTextView = tellUsTextView;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.tell_us_popup, null);

        m_chronicallyLate = (TextView) v.findViewById(R.id.tell_us_pop_up_chronically_late);
        m_usuallyLate = (TextView) v.findViewById(R.id.tell_us_pop_up_usually_late);
        m_sometimesLate = (TextView) v.findViewById(R.id.tell_us_pop_up_sometimes_late);
        m_usuallyOnTime = (TextView) v.findViewById(R.id.tell_us_pop_up_usually_on_time);
        m_neverLate = (TextView) v.findViewById(R.id.tell_us_pop_up_never_late);

        SetAllTextViewColors();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_tellUsTextView.setText(((TextView) v).getText().toString());
                dismiss();
            }
        };
        m_chronicallyLate.setOnClickListener(listener);
        m_usuallyLate.setOnClickListener(listener);
        m_sometimesLate.setOnClickListener(listener);
        m_usuallyOnTime.setOnClickListener(listener);
        m_neverLate.setOnClickListener(listener);

        return new AlertDialog.Builder(getActivity()).setView(v).create();
    }

    private void SetAllTextViewColors() {
        if(m_tellUsTextView==null || m_chronicallyLate==null ||  m_usuallyLate==null
                || m_sometimesLate ==null || m_usuallyOnTime==null || m_neverLate==null) return;

        m_chronicallyLate.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_usuallyLate.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_sometimesLate.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_usuallyOnTime.setTextColor(getResources().getColor(R.color.greyish_brown));
        m_neverLate.setTextColor(getResources().getColor(R.color.greyish_brown));

        if(m_tellUsTextView.getText().toString().equals(m_chronicallyLate.getText().toString())){
            m_chronicallyLate.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_tellUsTextView.getText().toString().equals(m_usuallyLate.getText().toString())){
            m_usuallyLate.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_tellUsTextView.getText().toString().equals(m_sometimesLate.getText().toString())){
            m_sometimesLate.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_tellUsTextView.getText().toString().equals(m_usuallyOnTime.getText().toString())){
            m_usuallyOnTime.setTextColor(getResources().getColor(R.color.greeny_blue));
        } else if(m_tellUsTextView.getText().toString().equals(m_neverLate.getText().toString())){
            m_neverLate.setTextColor(getResources().getColor(R.color.greeny_blue));
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

}
