package com.example.raz.lifeo.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ListView;

import com.example.raz.lifeo.Adapters.DiagramAdapter;
import com.example.raz.lifeo.Entities.EventsContainer;
import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DiagramActivity extends LifeoActionBarActivity {

    private List<L_Event> m_myEvents = new ArrayList<>();
    private ListView m_listViewEvents = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagram);

        loadMyEvents();
        InitUI();
    }

    private void loadMyEvents() {
        SharedPreferences mPrefs = getSharedPreferences(getResources().getString(R.string.default_tag), Context.MODE_MULTI_PROCESS);
        Gson gson = new Gson();
        String json = mPrefs.getString(getResources().getString(R.string.events_shared_prefences_tag),
                getResources().getString(R.string.empty).toString());
        if(json.equals(getResources().getString(R.string.empty).toString())) return;
        m_myEvents = gson.fromJson(json, EventsContainer.class).getEvents();
    }

    private void InitUI() {
        m_listViewEvents = (ListView) findViewById(R.id.diagram_listview);

        m_listViewEvents.setAdapter(new DiagramAdapter(this,m_myEvents,m_listViewEvents));
    }
}
