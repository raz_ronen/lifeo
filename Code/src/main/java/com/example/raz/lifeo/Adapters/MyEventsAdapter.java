package com.example.raz.lifeo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.example.raz.lifeo.Activities.MyEventsActivity;
import com.example.raz.lifeo.Activities.NewEventActivity;
import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Raz on 7/18/2017.
 */
public class MyEventsAdapter extends BaseSwipeAdapter {
    private static final int SWIPE_DURATION = 250;
    private static final int MOVE_DURATION = 150;
    HashMap<Long, Integer> mItemIdTopMap = new HashMap<Long, Integer>();

    private List<L_Event> m_myEvents = new ArrayList<>();
    private ListView m_listView = null;
    private Context mContext;

    public MyEventsAdapter(Context mContext, List<L_Event> m_myEvents, ListView listview) {
        this.mContext = mContext;
        this.m_myEvents = m_myEvents;
        this.m_listView = listview;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    //ATTENTION: Never bind listener or fill values in generateView.
    //           You have to do that in fillValues method.
    @Override
    public View generateView(int position, ViewGroup parent) {
        return LayoutInflater.from(mContext).inflate(R.layout.my_events_event_item, null);
    }

    @Override
    public void fillValues(final int position, final View convertView) {
        L_Event event = m_myEvents.get(position);
        TextView t = (TextView)convertView.findViewById(R.id.position);
        t.setText((position + 1 )+".");

        LinearLayout container = (LinearLayout) convertView.findViewById(R.id.my_event_event_container);

        container.getLayoutParams().width = getScreenWidth();
        container.requestLayout();

        TextView title = (TextView) convertView.findViewById(R.id.my_events_event_item_title);
        TextView date = (TextView) convertView.findViewById(R.id.my_events_event_item_date);
        TextView time = (TextView) convertView.findViewById(R.id.my_events_event_item_time);
        ImageButton deleteBtn = (ImageButton) convertView.findViewById(R.id.my_events_item_delete_button);

        if(title!=null && event.getTitle()!=null) title.setText(event.getTitle());
        if(date!=null && event.getDate()!=null) date.setText(event.getDate().substring(0,event.getDate().length()-5));
        if(time!=null && event.getDate()!=null) time.setText(event.getDate().substring(event.getDate().length()-5));

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation anim = AnimationUtils.loadAnimation(
                        mContext, android.R.anim.slide_out_right
                );
                anim.setDuration(800);

                View child = getCurrentChild(convertView);
                if(child==null) return;
                child.startAnimation(anim);

                new Handler().postDelayed(new Runnable() {

                    public void run() {
                        m_myEvents.remove(position);
                        animateRemoval(m_listView,convertView);
                        closeAllItems();

                        notifyDataSetChanged();

                    }

                }, anim.getDuration());
                notifyDataSetChanged();
            }

            private View getCurrentChild(View view) {
                for(int i = 0; i < m_listView.getChildCount(); i++){
                    if(view==m_listView.getChildAt(i)){
                        return m_listView.getChildAt(i);
                    }
                }
                return null;
            }
        });
        container.setOnClickListener(new View.OnClickListener() {
            L_Event event = m_myEvents.get(position);
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NewEventActivity.class);
                if(event!=null) {
                    intent.putExtra(mContext.getResources().getString(R.string.tags_event), event);
                }
                if(event.getLocationFrom()!=null) {
                    intent.putExtra(mContext.getResources().getString(R.string.tags_event_from),
                            event.getLocationFrom());
                }
                if(event.getLocationTo()!=null) {
                    intent.putExtra(mContext.getResources().getString(R.string.tags_event_to),
                            event.getLocationTo());
                }
                intent.putExtra(mContext.getResources().getString(R.string.event_num_tag).toString(), position);
                mContext.startActivity(intent);
            }
        });
    }

    private int getScreenWidth() {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    @Override
    public int getCount() {
        return m_myEvents.size();
    }

    @Override
    public Object getItem(int position) {
        return m_myEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * This method animates all other views in the ListView container (not including ignoreView)
     * into their final positions. It is called after ignoreView has been removed from the
     * adapter, but before layout has been run. The approach here is to figure out where
     * everything is now, then allow layout to run, then figure out where everything is after
     * layout, and then to run animations between all of those start/end positions.
     */
    private void animateRemoval(final ListView listview, View viewToRemove) {
        int firstVisiblePosition = listview.getFirstVisiblePosition();
        for (int i = 0; i < listview.getChildCount(); ++i) {
            View child = listview.getChildAt(i);
            if (child != viewToRemove) {
                int position = firstVisiblePosition + i;
                long itemId = getItemId(position);
                mItemIdTopMap.put(itemId, child.getTop());
            }
        }
        // Delete the item from the adapter
        int position = m_listView.getPositionForView(viewToRemove);
        final ViewTreeObserver observer = listview.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                observer.removeOnPreDrawListener(this);
                boolean firstAnimation = true;
                int firstVisiblePosition = listview.getFirstVisiblePosition();
                for (int i = 0; i < listview.getChildCount(); ++i) {
                    final View child = listview.getChildAt(i);
                    int position = firstVisiblePosition + i;
                    long itemId = getItemId(position);
                    Integer startTop = mItemIdTopMap.get(itemId);
                    int top = child.getTop();
                    if (startTop != null) {
                        if (startTop != top) {
                            int delta = startTop - top;
                            child.setTranslationY(delta);
                            child.animate().setDuration(MOVE_DURATION).translationY(0);
                            if (firstAnimation) {
                                child.animate().withEndAction(new Runnable() {
                                    public void run() {
                                    }
                                });
                                firstAnimation = false;
                            }
                        }
                    } else {
                        // Animate new views along with the others. The catch is that they did not
                        // exist in the start state, so we must calculate their starting position
                        // based on neighboring views.
                        int childHeight = child.getHeight() + listview.getDividerHeight();
                        startTop = top + (i > 0 ? childHeight : -childHeight);
                        int delta = startTop - top;
                        child.setTranslationY(delta);
                        child.animate().setDuration(MOVE_DURATION).translationY(0);
                        if (firstAnimation) {
                            child.animate().withEndAction(new Runnable() {
                                public void run() {
                                }
                            });
                            firstAnimation = false;
                        }
                    }
                }
                mItemIdTopMap.clear();
                return true;
            }
        });
    }
}