package com.example.raz.lifeo.Activities;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.raz.lifeo.Entities.EventsContainer;
import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.Fragments.ClockFragment;
import com.example.raz.lifeo.R;
import com.example.raz.lifeo.Sensors.OnSwipeTouchListener;
import com.example.raz.lifeo.Tools.SemiCircleDrawable;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends LifeoActionBarActivity {

    private static final String TAG = "HomeActivity.java";
    private List<L_Event> m_myEvents = new ArrayList<>();
    private Integer m_eventDisplayed = 0;
    //plus button
    private FloatingActionButton m_fabPlus = null;
    private FloatingActionButton m_fab1 = null;
    private FloatingActionButton m_fab2 = null;
    private FloatingActionButton m_fab3 = null;
    private Animation[] m_showFabAnimation = new Animation[3];
    private Animation[] m_hideFabAnimation = new Animation[3];
    private Boolean m_minFabsShown = false;

    //time raw data panel
    private TextView m_goToBedTextView = null;
    private TextView m_wakeUpTextView = null;
    private TextView m_hitTheRoadTextView = null;
    private TextView m_arrivingTextView = null;

    //top panel
    private ImageButton m_leftBtn = null;
    private ImageButton m_rightBtn = null;
    private TextView m_eventTitle = null;
    private TextView m_eventDate = null;

    private Fragment m_clockFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        loadMyEvents();

        InitUI();

        InitSensors();
    }

    private void InitSensors() {
        ((RelativeLayout) findViewById(R.id.home_container)).setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                displayOneEventBefore();
            }

            @Override
            public void onSwipeRight() {
                displayOneEventAfter();
            }
        });
    }

    private void loadMyEvents() {
        SharedPreferences mPrefs = getSharedPreferences(getResources().getString(R.string.default_tag), Context.MODE_MULTI_PROCESS);
        Gson gson = new Gson();
        String json = mPrefs.getString(getResources().getString(R.string.events_shared_prefences_tag),
                getResources().getString(R.string.empty).toString());
        if(json.equals(getResources().getString(R.string.empty).toString())) return;
        m_myEvents = gson.fromJson(json, EventsContainer.class).getEvents();
    }

    private void InitUI() {
        setTextViews();

        initUIComponents();

        setPlusButton();

    }

    private void initUIComponents() {
        m_leftBtn = (ImageButton) findViewById(R.id.home_left_arrow);
        m_rightBtn = (ImageButton) findViewById(R.id.home_right_arrow);
        m_eventTitle = (TextView) findViewById(R.id.home_event_title);
        m_eventDate = (TextView) findViewById(R.id.home_event_date);
        m_clockFragment = (Fragment)  getFragmentManager().findFragmentById(R.id.home_clock_fragment);

        if(m_myEvents==null || m_myEvents.size()==0){
            m_eventDate.setText(getResources().getString(R.string.empty).toString());
            m_eventTitle.setText(getResources().getString(R.string.empty).toString());
            m_leftBtn.setClickable(false);
            m_rightBtn.setClickable(false);
        } else {
            displayEvent(m_myEvents.get(m_eventDisplayed));
            if(m_clockFragment instanceof ClockFragment){
                ((ClockFragment) m_clockFragment).setSymbols(m_myEvents.get(m_eventDisplayed));
            }
        }

        m_leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayOneEventBefore();
            }
        });
        m_rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayOneEventAfter();
            }
        });
    }

    private void displayOneEventBefore(){
        if (m_myEvents == null || m_myEvents.size() == 0) return;
        m_eventDisplayed = (--m_eventDisplayed) % m_myEvents.size();
        if (m_eventDisplayed < 0) m_eventDisplayed += m_myEvents.size();
        displayEvent(m_myEvents.get(m_eventDisplayed));
    }

    private void displayOneEventAfter(){
        if(m_myEvents==null || m_myEvents.size()==0) return;
        m_eventDisplayed = (++m_eventDisplayed)%m_myEvents.size();
        displayEvent(m_myEvents.get(m_eventDisplayed));
    }

    private void displayEvent(L_Event l_event) {
        m_eventDate.setText(l_event.getDate().substring(0, l_event.getDate().indexOf(" ")));
        m_eventTitle.setText(l_event.getTitle());

        m_goToBedTextView.setText(l_event.getM_calculatedGoToBed());
        m_wakeUpTextView.setText(l_event.getM_calculatedWwakeUp());
        m_hitTheRoadTextView.setText(l_event.getM_calculatedHitRoad());
        m_arrivingTextView.setText(l_event.getM_calculatedArrival());

        if(m_clockFragment instanceof ClockFragment){
            ((ClockFragment) m_clockFragment).setSymbols(m_myEvents.get(m_eventDisplayed));
        }
    }

    private void setPlusButton() {
        m_fabPlus = (FloatingActionButton) findViewById(R.id.fab_plus);
        m_fab1 = (FloatingActionButton) findViewById(R.id.fab_1);
        m_fab2 = (FloatingActionButton) findViewById(R.id.fab_2);
        m_fab3 = (FloatingActionButton) findViewById(R.id.fab_3);

        m_showFabAnimation[0] = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show);
        m_showFabAnimation[1] = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_show);
        m_showFabAnimation[2] = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_show);
        m_hideFabAnimation[0] = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide);
        m_hideFabAnimation[1] = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_hide);
        m_hideFabAnimation[2] = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_hide);

        m_fabPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!m_minFabsShown) {
                    ShowMinFabs();
                    m_minFabsShown = true;
                } else {
                    m_minFabsShown = false;
                    CloseMinFabs();
                }
            }
        });
        m_fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseMinFabs();
                m_minFabsShown = false;
            }
        });
        m_fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseMinFabs();
                m_minFabsShown = false;
            }
        });
        m_fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseMinFabs();
                m_minFabsShown = false;
            }
        });
    }

    private void setTextViews() {
        m_goToBedTextView = (TextView) findViewById(R.id.home_textview_go_to_bed);
        m_wakeUpTextView = (TextView) findViewById(R.id.home_textview_wake_up);
        m_hitTheRoadTextView = (TextView) findViewById(R.id.home_textview_hit_road);
        m_arrivingTextView = (TextView) findViewById(R.id.home_textview_arriving_time);

        m_goToBedTextView.setText(getResources().getString(R.string.home_time_default).toString());
        m_wakeUpTextView.setText(getResources().getString(R.string.home_time_default).toString());
        m_hitTheRoadTextView.setText(getResources().getString(R.string.home_time_default).toString());
        m_arrivingTextView.setText(getResources().getString(R.string.home_time_default).toString());
    }


    private void ShowMinFabs() {
        m_fab1.startAnimation(m_showFabAnimation[0]);
        m_fab1.setClickable(true);

        m_fab2.startAnimation(m_showFabAnimation[1]);
        m_fab2.setClickable(true);

        m_fab3.startAnimation(m_showFabAnimation[2]);
        m_fab3.setClickable(true);
    }

    private void CloseMinFabs(){
        m_fab1.startAnimation(m_hideFabAnimation[0]);
        m_fab1.setClickable(false);

        m_fab2.startAnimation(m_hideFabAnimation[1]);
        m_fab2.setClickable(false);

        m_fab3.startAnimation(m_hideFabAnimation[2]);
        m_fab3.setClickable(false);
    }
}
