package com.example.raz.lifeo.Interfaces;

/**
 * Created by Raz on 7/20/2017.
 */
public interface Notified {

    void notify(String str, String tag);
}
