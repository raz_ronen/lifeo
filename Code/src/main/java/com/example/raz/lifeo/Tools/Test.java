package com.example.raz.lifeo.Tools;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.widget.RelativeLayout;

/**
 * Created by Raz on 7/26/2017.
 */
public class Test extends RelativeLayout {
    Context ctx;
    String colorString;
    int color;

    public Test(Context context) {
        super(context);
        this.ctx = context;
        color = Color.WHITE;
        this.setWillNotDraw(false);
        this.setPadding(10, 0, 10, 0);
        this.setLayoutParams(new LayoutParams(150, 150));
    }

    public Test(Context context, String color) {
        super(context);
        this.ctx = context;
        this.colorString = color;
        this.setWillNotDraw(false);
        this.setPadding(10, 0, 10, 0);
        this.setLayoutParams(new LayoutParams(150, 150));
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if(colorString != null)
            color = Color.parseColor("#" + colorString);

        int rectValue = 35;
        Log.d("Rounded", "rectValue: " + rectValue);
        RectF rect1 = new RectF(0, 0, rectValue, rectValue);

        Paint paint = new Paint();
        paint.setStrokeWidth(1);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(color);

        canvas.save();
        canvas.drawRoundRect(rect1, 10, 10, paint);
        canvas.restore();

        super.onDraw(canvas);
    }

    public void ChangeBackgroundColor(String colorString) {
        color = Color.parseColor(colorString);
        invalidate();
    }
}
