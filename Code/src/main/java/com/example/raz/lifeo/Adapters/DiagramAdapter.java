package com.example.raz.lifeo.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.R;

import java.util.List;

/**
 * Created by Raz on 7/22/2017.
 */
public class DiagramAdapter extends BaseAdapter {

    private List<L_Event> m_events = null;
    private ListView m_listview = null;
    private Activity m_activity;
    private LayoutInflater m_inflater;
    private Animation expandAnimation = null;

    public DiagramAdapter(Activity activity, List<L_Event> m_events, ListView listview) {
        this.m_events = m_events;
        this.m_activity = activity;
        this.m_listview = listview;
        this.expandAnimation = AnimationUtils.loadAnimation(m_activity, R.anim.diagram_bar_rise);
        m_inflater = m_activity.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return m_events.size();
    }

    @Override
    public Object getItem(int position) {
        return m_events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = m_inflater.inflate(R.layout.diagram_item, null);
        }
        final L_Event event = m_events.get(position);
        final TextView date = (TextView) view.findViewById(R.id.diagram_item_date);
        final TextView title = (TextView) view.findViewById(R.id.diagram_item_event_title);
        final LinearLayout barsContainer = (LinearLayout) view.findViewById(R.id.diagram_item_bars_container);
        final LinearLayout bar1_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_1_full);
        final LinearLayout bar2_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_2_full);
        final LinearLayout bar3_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_3_full);
        final LinearLayout bar4_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_4_full);
        final ImageButton share_btn = (ImageButton) view.findViewById(R.id.diagram_item_share_button);

        if (date != null && event.getDate() != null) date.setText(event.getDate());
        if (title != null && event.getTitle() != null) title.setText(event.getTitle());
        barsContainer.post(new Runnable() {
            public void run() {
                if (event.getSleep() != null)
                    setBarSize(event.getSleep(), bar1_full, barsContainer.getHeight());
                if (event.getWakeUp() != null)
                    setBarSize(event.getWakeUp(), bar2_full, barsContainer.getHeight());
                if (event.getArrival() != null)
                    setBarSize(event.getArrival(), bar3_full, barsContainer.getHeight());
                if (event.getAvg() != null)
                    setBarSize(event.getAvg(), bar4_full, barsContainer.getHeight());
            }
        });
        if (share_btn != null) share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(event.getTitle()));
                m_activity.startActivity(Intent.createChooser(sharingIntent, m_activity.getResources().getString(R.string.share_using_text)));
            }
        });

        setDiagramTextViewLevel(view, event);
        //view.addOnAttachStateChangeListener(listener);
        return view;
    }

    private void setDiagramTextViewLevel(View view, L_Event event) {
        TextView tsleep = (TextView) view.findViewById(R.id.diagram_item_bar_textview_sleep);
        TextView tWakeup = (TextView) view.findViewById(R.id.diagram_item_bar_textview_wakeup);
        TextView tArrival = (TextView) view.findViewById(R.id.diagram_item_bar_textview_arrival);
        TextView tAvg = (TextView) view.findViewById(R.id.diagram_item_bar_textview_avg);

        if(tsleep!=null && event.getSleep()!=null) tsleep.setText(String.valueOf(event.getSleep()/10));
        if(tWakeup!=null && event.getWakeUp()!=null) tWakeup.setText(String.valueOf(event.getWakeUp()/10));
        if(tArrival!=null && event.getArrival()!=null) tArrival.setText(String.valueOf(event.getArrival()/10));
        if(tAvg!=null && event.getAvg()!=null) tAvg.setText(String.valueOf(event.getAvg()/10));
    }

    private View.OnAttachStateChangeListener listener = new View.OnAttachStateChangeListener() {
        @Override
        public void onViewAttachedToWindow(View view) {
            final LinearLayout bar1_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_1_full);
            final LinearLayout bar2_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_2_full);
            final LinearLayout bar3_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_3_full);
            final LinearLayout bar4_full = (LinearLayout) view.findViewById(R.id.diagram_item_bar_4_full);
            bar1_full.startAnimation(expandAnimation);
            bar2_full.startAnimation(expandAnimation);
            bar3_full.startAnimation(expandAnimation);
            bar4_full.startAnimation(expandAnimation);
        }

        @Override
        public void onViewDetachedFromWindow(View v) {

        }
    };

    private void setBarSize(Integer percentage, LinearLayout bar_full, int yContainer) {
        if (bar_full == null) return;
        int y = (int) ((((percentage / 10) *10)/100.0f) * yContainer);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, y);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
        bar_full.setLayoutParams(lp);
    }
}
