package com.example.raz.lifeo.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.example.raz.lifeo.R;

public class ProfileActivity extends AppCompatActivity {

    private FloatingActionButton m_btnPlus = null;
    private FloatingActionButton m_btn1 = null;
    private FloatingActionButton m_btn2 = null;
    private Animation rotate = null;
    private Boolean menuOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initUI();
    }

    private void initUI() {
        m_btnPlus = (FloatingActionButton) findViewById(R.id.profile_btn);
        m_btn1 = (FloatingActionButton) findViewById(R.id.profile_btn_1);
        m_btn2 = (FloatingActionButton) findViewById(R.id.profile_btn_2);
        final FloatingActionButton expandingView = (FloatingActionButton) findViewById(R.id.view_expand);

        Button btn = (Button) findViewById(R.id.profile_to_home);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               GoToHome();
            }
        });

        rotate = AnimationUtils.loadAnimation(getApplication(), R.anim.rotate);
        final Animation expandView = AnimationUtils.loadAnimation(getApplication(), R.anim.expand_view);
        final Animation show1 = AnimationUtils.loadAnimation(getApplication(), R.anim.profile_btn1_show);
        final Animation show2 = AnimationUtils.loadAnimation(getApplication(), R.anim.profile_btn2_show);

        final Animation reverseRotate = AnimationUtils.loadAnimation(getApplication(), R.anim.reverse_rotate);
        final Animation unexpandView = AnimationUtils.loadAnimation(getApplication(), R.anim.unexpand_view);
        final Animation hide1 = AnimationUtils.loadAnimation(getApplication(), R.anim.profile_btn1_hide);
        final Animation hide2 = AnimationUtils.loadAnimation(getApplication(), R.anim.profile_btn2_hide);

        m_btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!menuOpen){
                    m_btnPlus.startAnimation(rotate);
                    expandingView.startAnimation(expandView);
                    m_btn1.startAnimation(show1);
                    m_btn2.startAnimation(show2);
                    menuOpen = true;
                } else {
                    m_btnPlus.startAnimation(reverseRotate);
                    expandingView.startAnimation(unexpandView);
                    m_btn1.startAnimation(hide1);
                    m_btn2.startAnimation(hide2);
                    menuOpen = false;
                }
            }
        });
    }

    private void GoToHome() {
        startActivity(new Intent(this,HomeActivity.class));
    }
}
