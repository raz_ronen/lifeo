package com.example.raz.lifeo.Activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raz.lifeo.Components.LifeoDatePicker;
import com.example.raz.lifeo.Fragments.LoginPanel;
import com.example.raz.lifeo.R;

import java.util.Calendar;

public class LoginActivity extends Activity {

    private Fragment m_loginPanel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.activity_login);

        initUI();
    }

    private void initUI() {
        m_loginPanel = (Fragment) getFragmentManager().findFragmentById(R.id.login_login_panel);

        initButtonsListeners();
    }

    private void initButtonsListeners() {

        ((Button) findViewById(R.id.login_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((LoginPanel) m_loginPanel).name();
                String birthday = ((LoginPanel) m_loginPanel).birthday();
                if(name!=null) Log.d("Name", name);
                if(birthday!=null) Log.d("Birthday", birthday);
                Toast.makeText(getApplicationContext(), "Logining..", Toast.LENGTH_SHORT).show();
            }
        });

        ((ImageButton) findViewById(R.id.login_facebook_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Logining to Facebook..", Toast.LENGTH_SHORT).show();
            }
        });

        ((TextView) findViewById(R.id.login_sign_up_textview)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });
    }

}
