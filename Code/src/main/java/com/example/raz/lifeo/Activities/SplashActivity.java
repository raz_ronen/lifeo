package com.example.raz.lifeo.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.example.raz.lifeo.R;

public class SplashActivity extends Activity {

    private static final long SLEEP_TIME = 2400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        Animate();

        KillSplash();
    }

    private void Animate() {
        ImageView person = (ImageView) findViewById(R.id.splash_person);
        ImageView  lines= (ImageView) findViewById(R.id.splash_lines);
        ImageView text = (ImageView) findViewById(R.id.splash_text);


        person.startAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_person_slide_in));
        lines.startAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_lines_fade_in));
        text.startAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_text_fade_in));
    }

    private void KillSplash() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(SLEEP_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }).start();
    }
}
