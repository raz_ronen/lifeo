package com.example.raz.lifeo.Activities;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raz.lifeo.Components.TellUsPopUp;
import com.example.raz.lifeo.R;

public class TellUsActivity extends LifeoActionBarActivity {

    private RelativeLayout m_lateChooser = null;
    private SeekBar m_fallAsleepSeekBar = null;
    private SeekBar m_morningRoutineSeekBar = null;
    private RelativeLayout m_progressBarToast = null;
    private TextView m_toastTextView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tell_us);

        InitUI();
    }

    private void InitUI() {
        m_lateChooser = (RelativeLayout) findViewById(R.id.tell_us_choose_latency);
        m_fallAsleepSeekBar = (SeekBar) findViewById(R.id.tell_us_fall_asleep_progress_bar);
        m_morningRoutineSeekBar = (SeekBar) findViewById(R.id.tell_us_progress_bar_morning_routine);
        m_progressBarToast = (RelativeLayout) findViewById(R.id.tell_us_progress_bar_toast);
        m_toastTextView = (TextView) findViewById(R.id.tell_us_progress_bar_toast_textview);

        m_lateChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TellUsPopUp tellUsPopUp = new TellUsPopUp();
                tellUsPopUp.setTellUsTextView(((TextView) findViewById(R.id.tell_us_late_text_view)));
                tellUsPopUp.show(getFragmentManager(), getResources().getString(R.string.default_tag));
            }
        });

        ((ImageButton) findViewById(R.id.tell_us_lets_start_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TellUsActivity.this,HomeActivity.class));
            }
        });

        m_fallAsleepSeekBar.setOnSeekBarChangeListener(new TellUsSeekBarListener());
        m_morningRoutineSeekBar.setOnSeekBarChangeListener(new TellUsSeekBarListener());
    }

    class TellUsSeekBarListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            int coord[] = {0,0};
            seekBar.getLocationInWindow(coord);
            Log.d("HEIGH", String.valueOf(coord[1]));
            RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(AbsoluteLayout.LayoutParams.WRAP_CONTENT, AbsoluteLayout.LayoutParams.WRAP_CONTENT);
            llp.setMargins(((int)seekBar.getX()) + 40 + (int)((progress / ((float)200) * seekBar.getWidth())),
                    coord[1]-400,
                    0, 0);
            m_progressBarToast.setLayoutParams(llp);
            m_toastTextView.setText(String.valueOf(progress));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            m_progressBarToast.setVisibility(View.VISIBLE);
            m_progressBarToast.setAlpha(0.0f);
            m_progressBarToast.animate().alpha(1.0f);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            m_progressBarToast.setAlpha(1.0f);
            m_progressBarToast.animate().alpha(0.0f);
        }
    }
}
