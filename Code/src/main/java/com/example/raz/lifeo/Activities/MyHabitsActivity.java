package com.example.raz.lifeo.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.raz.lifeo.R;

public class MyHabitsActivity extends LifeoActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_habits);
    }
}
