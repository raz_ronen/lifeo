package com.example.raz.lifeo.Components;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.raz.lifeo.Activities.LoginActivity;
import com.example.raz.lifeo.R;

/**
 * Created by Raz on 7/16/2017.
 */
public class LifeoDatePicker extends DialogFragment {
    private Button m_OK = null;
    private Button m_Back = null;
    private DatePicker m_datePicker = null;
    private EditText m_birthdayEditText = null;
    private Boolean writeBirthday = true;

    public void setDatePicker(EditText birthdayEditText){
        this.m_birthdayEditText = birthdayEditText;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.lifeo_date_picker, null);

        m_OK = (Button) v.findViewById(R.id.lifeo_data_picker_ok);
        m_Back = (Button) v.findViewById(R.id.lifeo_data_picker_back);
        m_datePicker = (DatePicker) v.findViewById(R.id.lifeo_data_picker_picker);

        m_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeBirthday = false;
                exit();
            }
        });
        m_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit();
            }
        });

        return new AlertDialog.Builder(getActivity()).setView(v).create();
    }

    private void exit() {
        getDialog().dismiss();
    }

    private void writeBirthday(){
        if(m_OK==null || !writeBirthday) return;
        if (m_birthdayEditText != null) {
            String birthday = m_datePicker.getMonth() + "/"
                    + m_datePicker.getDayOfMonth() + "/"
                    + m_datePicker.getYear();
            m_birthdayEditText.setText(birthday);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        writeBirthday();
        super.onDismiss(dialog);
    }
}
