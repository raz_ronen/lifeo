package com.example.raz.lifeo.Activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raz.lifeo.Fragments.LoginPanel;
import com.example.raz.lifeo.R;

public class SignUpActivity extends Activity {

    private Fragment m_loginPanel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initUI();
    }

    private void initUI() {
        m_loginPanel = (Fragment) getFragmentManager().findFragmentById(R.id.sign_up_login_panel);

        ((Button) findViewById(R.id.sign_up_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((LoginPanel) m_loginPanel).name();
                String birthday = ((LoginPanel) m_loginPanel).birthday();
                if(name!=null) Log.d("Name", name);
                if(birthday!=null) Log.d("Birthday", birthday);
                startActivity(new Intent(SignUpActivity.this, TellUsActivity.class));
            }
        });

        ((ImageButton) findViewById(R.id.login_facebook_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Logining to Facebook..", Toast.LENGTH_SHORT).show();
            }
        });

        ((TextView) findViewById(R.id.sign_up_login_textview)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });

        ((TextView) findViewById(R.id.sign_up_terms_and_conditions)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, TermsAndConditions.class));
            }
        });
    }
}
