package com.example.raz.lifeo.Fragments;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.raz.lifeo.Activities.AboutActivity;
import com.example.raz.lifeo.Activities.DiagramActivity;
import com.example.raz.lifeo.Activities.LifeoActionBarActivity;
import com.example.raz.lifeo.Activities.MyEventsActivity;
import com.example.raz.lifeo.Activities.MyHabitsActivity;
import com.example.raz.lifeo.Activities.NewEventActivity;
import com.example.raz.lifeo.Activities.SettingsActivity;
import com.example.raz.lifeo.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * A simple {@link Fragment} subclass.
 */
public class Menu extends Fragment {

    private static final long REGULAR_DURATION_LENGTH = 1000;
    private static final long SHORT_DURATION_LENGTH = 400;
    List<LinearLayout> m_options = new ArrayList<>();
    private View view = null;

    public Menu() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_menu, container, false);

        initOptions(view);
        return view;
    }

    private void initOptions(View v) {
        m_options.add((LinearLayout) v.findViewById(R.id.lifeo_menu_option_new_event));
        m_options.add((LinearLayout) v.findViewById(R.id.lifeo_menu_option_my_events));
        m_options.add((LinearLayout) v.findViewById(R.id.lifeo_menu_option_diagrama));
        m_options.add((LinearLayout) v.findViewById(R.id.lifeo_menu_option_my_habits));
        m_options.add((LinearLayout) v.findViewById(R.id.lifeo_menu_option_settings));
        m_options.add((LinearLayout) v.findViewById(R.id.lifeo_menu_option_about));

        setAllClickListeners();
    }

    private void setAllClickListeners() {
        setOnClickListener(m_options.get(0), new Intent(getActivity(), NewEventActivity.class));
        setOnClickListener(m_options.get(1), new Intent(getActivity(), MyEventsActivity.class));
        setOnClickListener(m_options.get(2), new Intent(getActivity(), DiagramActivity.class));
        setOnClickListener(m_options.get(3), new Intent(getActivity(), MyHabitsActivity.class));
        setOnClickListener(m_options.get(4), new Intent(getActivity(), SettingsActivity.class));
        setOnClickListener(m_options.get(5), new Intent(getActivity(), AboutActivity.class));
    }

    private void setOnClickListener(LinearLayout linearLayout, final Intent intent) {
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });
    }

    public void fadeIn() {
        for (int i = 0; i < m_options.size(); i++) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.open_menu_option_default);
            fadeIn.setFillAfter(true);
            fadeIn.setStartOffset(i * 200);
            m_options.get(i).startAnimation(fadeIn);
        }
    }

    public void fadeOut() {
        for (int i = 0; i < m_options.size(); i++) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.close_menu_option_default);
            fadeOut.setFillAfter(true);
            fadeOut.setDuration(SHORT_DURATION_LENGTH);
            fadeOut.setStartOffset(m_options.size() * 100 - i * 100);
            m_options.get(i).startAnimation(fadeOut);
        }
    }

}