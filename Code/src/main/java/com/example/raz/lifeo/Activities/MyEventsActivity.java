package com.example.raz.lifeo.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.raz.lifeo.Adapters.MyEventsAdapter;
import com.example.raz.lifeo.Entities.EventsContainer;
import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MyEventsActivity extends LifeoActionBarActivity {

    private List<L_Event> m_myEvents = new ArrayList<>();
    private ListView m_listViewEvents = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_events);

        loadMyEvents();
        InitUI();
        initAdapter();
    }

    private void loadMyEvents() {
        SharedPreferences mPrefs = getSharedPreferences(getResources().getString(R.string.default_tag), Context.MODE_MULTI_PROCESS);
        Gson gson = new Gson();
        String json = mPrefs.getString(getResources().getString(R.string.events_shared_prefences_tag),
                getResources().getString(R.string.empty).toString());
        if(json.equals(getResources().getString(R.string.empty).toString())) return;
        m_myEvents = gson.fromJson(json, EventsContainer.class).getEvents();
    }

    private void InitUI() {
        m_listViewEvents = (ListView) findViewById(R.id.my_events_list_view);
    }

    private void initAdapter() {

        if(m_listViewEvents!=null) m_listViewEvents.setAdapter(new MyEventsAdapter(this,m_myEvents,m_listViewEvents));
    }
}
