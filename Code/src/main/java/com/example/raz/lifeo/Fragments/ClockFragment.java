package com.example.raz.lifeo.Fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.raz.lifeo.Entities.L_Event;
import com.example.raz.lifeo.R;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClockFragment extends Fragment {

    private final static float CENTER_DIFF_WIDTH = 0.615f;
    private final static float CENTER_DIFF_HEIGHT = 0.51f;
    private final static float BIG_CLOCK_RADIOUS = 0.47f;
    private final static float SMALL_CLOCK_RADIOUS = 0.35f;

    private RelativeLayout m_container;
    private ImageView m_clockPaint;
    private ImageView m_bigEdge;
    private ImageView m_smallEdge;
    private ImageView m_zzz;
    private ImageView m_man;
    private ImageView m_roster;
    private ImageView m_car;

    public ClockFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clock, container, false);

        initUI(view);
        return view;
    }

    private void initUI(View view) {
        m_container = (RelativeLayout) view.findViewById(R.id.clock_container);
        m_clockPaint = (ImageView) view.findViewById(R.id.clock_paint);
        m_bigEdge = (ImageView) view.findViewById(R.id.clock_big_edge);
        m_smallEdge = (ImageView) view.findViewById(R.id.clock_small_edge);
        m_zzz = (ImageView) view.findViewById(R.id.clock_symbol_zzz);
        m_man = (ImageView) view.findViewById(R.id.clock_symbol_man);
        m_roster = (ImageView) view.findViewById(R.id.clock_symbol_roster);
        m_car = (ImageView) view.findViewById(R.id.clock_symbol_car);

        hideSymbols();
        setEdgesTime();
    }

    private void hideSymbols() {
        m_zzz.setVisibility(View.INVISIBLE);
        m_man.setVisibility(View.INVISIBLE);
        m_roster.setVisibility(View.INVISIBLE);
        m_car.setVisibility(View.INVISIBLE);
    }

    private void showSymbols() {
        m_zzz.setVisibility(View.VISIBLE);
        m_man.setVisibility(View.VISIBLE);
        m_roster.setVisibility(View.VISIBLE);
        m_car.setVisibility(View.VISIBLE);
    }

    public void setSymbols(L_Event event) {
        if(event==null) return;
        showSymbols();
        setSymbol(m_zzz, event.getM_calculatedGoToBed());
        setSymbol(m_man, event.getM_calculatedArrival());
        setSymbol(m_roster, event.getM_calculatedWwakeUp());
        setSymbol(m_car,event.getM_calculatedHitRoad());

    }

    private void setSymbol(ImageView symbol, String time) {
        if(time==null) return;
        int hour = Integer.parseInt(time.substring(0, 2));
        int min = Integer.parseInt(time.substring(3,5));

        int size = m_clockPaint.getDrawable().getIntrinsicHeight();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        int centerWidth = (int)(size*CENTER_DIFF_WIDTH);
        int centerHeight = (int)(size*CENTER_DIFF_HEIGHT);
        double[] diffs = getTriangleEdges((hour > 12)?size*BIG_CLOCK_RADIOUS:size*SMALL_CLOCK_RADIOUS,
                getTimeAngle(hour,min));
        params.leftMargin = centerWidth + (int) diffs[0];
        params.topMargin = centerHeight - (int) diffs[1];
        symbol.setLayoutParams(params);
    }

    private double[] getTriangleEdges(float edge, float timeAngle) {
        double[] edges = new double[2];
        edges[0] = Math.sin(Math.toRadians(timeAngle)) * edge;
        edges[1] = Math.cos(Math.toRadians(timeAngle)) * edge;
        return edges;
    }

    private void setEdgesTime() {
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int currentMin = rightNow.get(Calendar.MINUTE);

        float smallEdgeAngle = getTimeAngle(currentHour,currentMin);
        float bigEdgeAngle = (currentMin / 60.0f) * 360;
        Bitmap smallBM = BitmapFactory.decodeResource(getResources(),
                R.mipmap.clock_small_end);
        Bitmap bigBM = BitmapFactory.decodeResource(getResources(),
                R.mipmap.clock_big_end);
        m_smallEdge.setImageBitmap(rotate(smallBM, smallEdgeAngle));
        m_bigEdge.setImageBitmap(rotate(bigBM, bigEdgeAngle));
    }

    private float getTimeAngle(int hour, int min){
        return ((hour%12 + min / 60.0f) / 12.0f) * 360;
    }


    Bitmap rotate(Bitmap src, float degree)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(),
                matrix, true);
    }
}
