package com.example.raz.lifeo.Activities;

import android.os.Bundle;

import com.example.raz.lifeo.R;

public class AboutActivity extends LifeoActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
