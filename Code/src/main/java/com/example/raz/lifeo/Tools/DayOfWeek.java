package com.example.raz.lifeo.Tools;

import android.content.Context;

import com.example.raz.lifeo.R;

/**
 * Created by Raz on 7/18/2017.
 */
public class DayOfWeek {

    public static String Day(Context context, int dayOfMonth) {
        switch (dayOfMonth){
            case 0:
                return context.getResources().getString(R.string.sunday_text);
            case 1:
                return context.getResources().getString(R.string.monday_text);
            case 2:
                return context.getResources().getString(R.string.tuesday_text);
            case 3:
                return context.getResources().getString(R.string.wednesday_text);
            case 4:
                return context.getResources().getString(R.string.thursday_text);
            case 5:
                return context.getResources().getString(R.string.friday_text);
            case 6:
                return context.getResources().getString(R.string.saturday_text);
            default:
                return "none";
        }
    }
}
