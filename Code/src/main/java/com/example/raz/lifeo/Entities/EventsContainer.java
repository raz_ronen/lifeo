package com.example.raz.lifeo.Entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raz on 7/25/2017.
 */
public class EventsContainer {
    private List<L_Event> events = new ArrayList<>();

    public EventsContainer(List<L_Event> events) {
        this.events = events;
    }

    public List<L_Event> getEvents() {

        return events;
    }

    public void setEvents(List<L_Event> events) {
        this.events = events;
    }
}
